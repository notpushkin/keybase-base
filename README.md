# Keybase Base

A Minideb-based base image with Keybase (`stable-slim` version) preinstalled.

Usage:

```dockerfile
FROM registry.gitlab.com/notpushkin/keybase-base
RUN install_packages anything-you-need-from-debian-repo
COPY your-app /app/
CMD entrypoint.sh /app/your-app
```

This will start Keybase daemon and then run `/app/your-app`.

For a complete example, see [keybase-pdrbot].

[keybase-pdrbot]: https://gitlab.com/notpushkin/keybase-pdrbot
