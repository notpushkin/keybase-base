ARG BASE_IMAGE=keybaseio/client:stable-slim
FROM $BASE_IMAGE AS base

FROM bitnami/minideb:latest
LABEL maintainer="Alexander Pushkov <alexander@notpushk.in>"

RUN install_packages gnupg2 procps ca-certificates tini gosu

COPY --from=base /usr/bin/entrypoint.sh /usr/bin/entrypoint.sh
RUN chmod +x /usr/bin/entrypoint.sh

RUN groupadd --gid 1000 keybase && useradd --uid 1000 --gid keybase --shell /bin/bash --create-home keybase
VOLUME [ "/home/keybase/.config/keybase", "/home/keybase/.cache/keybase" ]

COPY --from=base /usr/bin/keybase /usr/bin/keybase
COPY --from=base /usr/bin/keybase.sig /usr/bin/keybase.sig

ENTRYPOINT ["tini", "--", "entrypoint.sh"]
